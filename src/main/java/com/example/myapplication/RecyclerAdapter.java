package com.example.myapplication;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.LocaleDisplayNames;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {


    private List<Team> teamList = new ArrayList<Team>();
    private Context context;

    public RecyclerAdapter(Context context, List<Team> teamList) {
        this.teamList = teamList;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.team_item,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        final Team team = MainActivity.dbHelper.getAllTeams().get(position);
        Log.d("Adapter","name : "+team.getName());
        holder.nameTeam.setText(team.getName());
        holder.teamLeague.setText(team.getLeague());
        new DownloadImagesTask(holder.imageTeam,team).execute();
        //holder.imageTeam.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier(animal.getImgFile(), "drawable", context.getPackageName())));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,TeamActivity.class);
                intent.putExtra("Team",team);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        Log.d("getItemCount", ""+teamList.size());
        return teamList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageTeam;
        TextView nameTeam;
        TextView teamLeague;
        RelativeLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageTeam = itemView.findViewById(R.id.imageA);
            nameTeam = itemView.findViewById(R.id.nameT);
            teamLeague = itemView.findViewById(R.id.league);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }

    public static class DownloadImagesTask extends AsyncTask<ImageView, Void, Bitmap> {
        ImageView imageV;
        Team team;
        public DownloadImagesTask(ImageView imageTeam, Team team) {
            this.imageV=imageTeam;
            this.team=team;
        }

        @Override
        protected Bitmap doInBackground(ImageView... imageViews) {

            Bitmap bmp = null;

            try {
                URL url =new URL(this.team.getTeamBadge());
                Log.d("Download image","url :"+url);
                URLConnection URLc =url.openConnection();
                InputStream input = URLc.getInputStream();
                Log.d("Download image",""+input);
                bmp = BitmapFactory.decodeStream(input);
            } catch (Exception e){
                Log.d("Download image","error"+e);
            }
            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap res){

            imageV.setImageBitmap(res);
        }
    }
}
