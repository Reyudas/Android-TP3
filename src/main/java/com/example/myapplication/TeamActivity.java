package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;
    public static String DONE="Done";
    public static String FAILED="Fail";

    public static String loadContent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context){

        HttpsURLConnection urlConnection =null;
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo network = manager.getActiveNetworkInfo();

        try {
            if (network !=null){

                URL url =WebServiceUrl.buildSearchTeam(team.getName());

                urlConnection =(HttpsURLConnection) url.openConnection();

                if(urlConnection.getResponseCode()==200){
                    InputStream res = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStream(res);
                } else {
                    Log.d("Timeout","Timeout");
                    return TeamActivity.FAILED;
                }

            }
            else {
                Log.d("Timeout", "no connection");
                return TeamActivity.FAILED;
            }
        }

        catch (Exception e){
            return TeamActivity.FAILED;
        }
        finally{

            if (urlConnection !=null){
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }
    }

    public static String loadLastEvent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {


        HttpsURLConnection urlConnection =null;
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo network = manager.getActiveNetworkInfo();

        try {
            if (network !=null){

                URL url =WebServiceUrl.buildSearchLastEvents(team.getIdTeam());

                Log.d("TEAMLAST","ici");
                urlConnection =(HttpsURLConnection) url.openConnection();

                if(urlConnection.getResponseCode()==200){
                    Log.d("TEAMLAST","ici2");
                    InputStream res = urlConnection.getInputStream();
                    responseHandlerTeam.readLastJsonStream(res);
                } else {
                    Log.d("Timeout","Timeout");
                    return TeamActivity.FAILED;
                }

            }
            else {
                Log.d("Timeout", "no connection");
                return TeamActivity.FAILED;
            }
        }

        catch (Exception e){
            return TeamActivity.FAILED;
        }
        finally{

            if (urlConnection !=null){
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }
    }


    public static String loadRank(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context){
        Log.d("RANK","OK");
        HttpsURLConnection urlConnection =null;
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo network = manager.getActiveNetworkInfo();

        try {
            if (network !=null){

                URL url =WebServiceUrl.buildGetRanking(team.getIdLeague());

                urlConnection =(HttpsURLConnection) url.openConnection();
                Log.d("RANK","la");
                if(urlConnection.getResponseCode()==200){
                    Log.d("RANK","ici");
                    InputStream res = urlConnection.getInputStream();
                    responseHandlerTeam.readRankJsonStream(res);
                    Log.d("RANK","DONE");
                } else {
                    Log.d("RANK","Timeout");
                    return TeamActivity.FAILED;
                }
            }
            else {
                Log.d("RANK", "no connection");
                return TeamActivity.FAILED;
            }
        }

        catch (Exception e){
            Log.d("RANK",""+e);
            return TeamActivity.FAILED;
        }
        finally{

            if (urlConnection !=null){
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra("Team");

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                RefreshTeamContent task=new RefreshTeamContent();
                task.execute(team);

                MainActivity.nbTreads++;

                updateView();

            }
        });

    }

    private final class RefreshTeamContent extends AsyncTask<Team, Void, String> {

        @Override
        protected String doInBackground(Team... params) {

            Team team = params[0];

            JSONResponseHandlerTeam responseHandlerTeam = new JSONResponseHandlerTeam(team);

            TeamActivity.loadContent(team, responseHandlerTeam, TeamActivity.this);

            TeamActivity.loadLastEvent(team, responseHandlerTeam, TeamActivity.this);

            TeamActivity.loadRank(team, responseHandlerTeam, TeamActivity.this);

            MainActivity.dbHelper.updateTeam(team);

            return TeamActivity.DONE;
        }

        @Override
        protected void onPostExecute(String result) {

            MainActivity.nbTreads--;

            Log.d("Maintread","nbtread"+MainActivity.nbTreads);

            if (MainActivity.nbTreads <= 0) {

                updateView();

                Log.d("Refresh","Fini");
            }
        }
    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        super.onBackPressed();
    }

    private void updateView() {

        new RecyclerAdapter.DownloadImagesTask(imageBadge,team).execute();
        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

    }

}
