package com.example.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    static SportDbHelper dbHelper;
    static int nbTreads;
    RecyclerView recyclerView;
    ListView listView;
    static RecyclerAdapter adapter;
    static SwipeRefreshLayout swipe;

    Cursor cur = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        dbHelper = new SportDbHelper(this);

        //dbHelper.populate();

        List<Team> teamL = new ArrayList<Team>();

        teamL = dbHelper.getAllTeams();


        Log.d("Main","Team list"+teamL);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);

        registerForContextMenu(recyclerView);

        adapter = new RecyclerAdapter(this,teamL );

        //listView = (ListView) findViewById(R.id.listView);

        /*SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllTeams(),
                new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.
                        COLUMN_LEAGUE_NAME },
                new int[] { android.R.id.text1, android.R.id.text2});

        listView.setAdapter(adapter);*/

        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        swipe.setOnRefreshListener(this);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivityForResult(intent,1);
            }
        });


        ItemTouchHelper.SimpleCallback remove = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                int position = viewHolder.getAdapterPosition();
                long id = MainActivity.dbHelper.getAllTeams().get(position).getId();

                MainActivity.dbHelper.deleteTeam((int)id);

                adapter.notifyDataSetChanged();
            }
        };
        new ItemTouchHelper(remove).attachToRecyclerView(recyclerView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {

        Log.d("debug","la");
        for (Team team : MainActivity.dbHelper.getAllTeams()){
            RefreshTeamContent task=new RefreshTeamContent();
            task.execute(team);

            MainActivity.nbTreads++;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    private final class RefreshTeamContent extends AsyncTask<Team, Void, String> {

        @Override
        protected String doInBackground(Team... params) {

            Team team = params[0];

            JSONResponseHandlerTeam responseHandlerTeam = new JSONResponseHandlerTeam(team);

            TeamActivity.loadContent(team, responseHandlerTeam, MainActivity.this);

            TeamActivity.loadLastEvent(team, responseHandlerTeam, MainActivity.this);

            TeamActivity.loadRank(team, responseHandlerTeam, MainActivity.this);

            dbHelper.updateTeam(team);

            return TeamActivity.DONE;
        }

        @Override
        protected void onPostExecute(String result) {

            MainActivity.nbTreads--;

            Log.d("Maintread","nbtread"+nbTreads);

            if (MainActivity.nbTreads <= 0) {

                MainActivity.adapter.notifyDataSetChanged();

                MainActivity.swipe.setRefreshing(false);

                Log.d("Refresh","Fini");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d("ADDTEST","Problem");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK){
            Team team = (Team) data.getParcelableExtra(Team.TAG);

            if (team != null){
                MainActivity.dbHelper.addTeam(team);
                MainActivity.adapter.notifyDataSetChanged();
                Log.d("ADDTEST","Problem");
            }
            else {
                Log.d("ADDTEST","Problem");
            }
        }
    }
}
