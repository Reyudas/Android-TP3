package com.example.myapplication;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;



/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeam {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerTeam(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("teams")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    if (name.equals("idTeam")) {
                        team.setIdTeam(reader.nextLong());
                    } else if (name.equals("strTeam")) {
                        team.setName(reader.nextString());
                    } else if (name.equals("strLeague")) {
                        team.setLeague(reader.nextString());
                    } else if (name.equals("idLeague")) {
                        team.setIdLeague(reader.nextLong());
                    } else if (name.equals("strStadium")) {
                        team.setStadium(reader.nextString());
                    } else if (name.equals("strStadiumLocation")) {
                        team.setStadiumLocation(reader.nextString());
                    } else if (name.equals("strTeamBadge")) {
                        team.setTeamBadge(reader.nextString());
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

    public void readRankJsonStream(InputStream response) throws IOException {
        Log.d("JSON","la");
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readRank(reader);
        } finally {
            reader.close();
        }
    }

    public void readRank(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayRank(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    public void readArrayRank(JsonReader reader) throws IOException {
        reader.beginArray();
        Log.d("RANK","here");
        int i=0;
        while (reader.hasNext()){
            i++;
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();

                if (name !=null && name.equals("name")){
                    String teamName = reader.nextString();
                    Log.d("RANK","TEAM "+teamName);
                    Log.d("RANK","TEAMExpect"+team.getName());

                    if(teamName.equals(team.getName())){
                        Log.d("RANK","here2");
                        team.setRanking(i);
                        while (reader.hasNext()){

                            String name2 =reader.nextName();
                            if(name2.equals("total")){
                                team.setTotalPoints(reader.nextInt());
                                break;
                            } else {
                                reader.skipValue();
                            }
                        }
                    }
                }
                else {
                    if (reader.hasNext()){
                        reader.skipValue();
                    }
                }
                Log.d("RANK","here3");

            }
            reader.endObject();
        }
        reader.endArray();
    }

    public void readLastJsonStream(InputStream response) throws IOException {
        Log.d("TEAMLAST","here");
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readLast(reader);
        } finally {
            reader.close();
        }
    }

    public void readLast(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            Log.d("TEAMLAST","here3"+name);
            if (name.equals("results")) {
                Log.d("TEAMLAST","here4");
                readArrayLast(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    private void readArrayLast(JsonReader reader) throws IOException {
        Log.d("TEAMLAST","here2");
        reader.beginArray();
        Match match = new Match();
        int nb = 0;
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                    } else  if (name.equals("strHomeTeam")){
                        match.setHomeTeam(reader.nextString());
                    } else if (name.equals("strAwayTeam")){
                        match.setAwayTeam(reader.nextString());
                    } else if (name.equals("intHomeScore")){
                        match.setHomeScore(reader.nextInt());
                    } else if (name.equals("intAwayScore")){
                        match.setAwayScore(reader.nextInt());
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
        team.setLastEvent(match);
        Log.d("TEAMLAST",""+team.getLastEvent().getAwayTeam());
    }

}
